# MineGuard

#### A real-time cryptocurrency mining detection tool. [Paper](http://tahir2.web.engr.illinois.edu/publications/RAID-cloud-mining.pdf).

## License

University of Illinois/NCSA
Open Source License

Copyright © 2017, University of Illinois at Urbana-Champaign. All rights reserved.

Developed by:

Muhammad Huzaifa

Mohammad Ahmad

University of Illinois at Urbana-Champaign


* Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal with the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimers.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimers in the documentation and/or other materials provided with the distribution.
* Neither the names of Muhammad Huzaifa, Mohammad Ahmad, University of Illinois at Urbana-Champaign, nor the names of its contributors may be used to endorse or promote products derived from this Software without specific prior written permission.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.

## Contact

This software was developed by Muhammad Huzaifa and Mohammad Ahmad.
Please contact Muhammad Huzaifa ([huzaifa2@illinois.edu](huzaifa2@illinois.edu)) or Mohammad Ahmad ([mahmad11@illinois.edu](mahmad11@illinois.edu)) for any questions, comments, or feedback.

## Instructions

### Directory Structure
* andres/ contains the random forest library that we used for classification. [Github](https://github.com/bjoern-andres/random-forest).
* classifier/ contains the training and prediction code used by MineGuard.
* data/ contains several tests (test\*.csv) and training data (\*-training.csv). It also contains ASCII versions of the OS and VM signatures (\*-signature.csv) obtained after training MineGuard on the given training data.
* scripts/ contains Bash and Python scripts that integrate everything to create MineGuard. monitor.sh is the "main" script that profiles and classifies a process or a VM. test.sh runs the test vectors from data/.

### Compilation Instructions
The code requires *C++11* support. To compile, simply do:
```
cd classifier
make
```

This will create the training and prediction binaries. This is the only required compilation (the rest of the code is in scripting languages).

### Training
To train the classifier, do:
```
cd classifier
./train <training_file> <signature_file>
```

**training_file** is a file containing the training data. An example is shown in data/\*-training.csv.

**signature_file** is the output file to which a serialized version of the random forest will be stored. This will be used in the classification process.

### Online Monitoring
Once the classifier has been compiled and trained, any process or VM can be profiled as follows:
```
cd scripts
./monitor.sh <PID>
```

**PID** is the process ID of a process or a VM. Currently, the path of the signature is hardcoded in monitor.sh and will need to be changed based on where the signature is located.

### Miscellaneous
To run the test vectors in data/, do:
```
cd scripts
./test.sh
```