#!/bin/bash

# check inputs and pring usage if need be
if [ -z "$1" ] || [ -z "$2" ] || [ -z "$3" ] ; then
	echo "usage: profile <Process PID> <Profile time> <Results file>"
	exit
fi

# parameters
pid=$1
running_time=$2
results=$3
sampling_time=2000 # 2 seconds
events=cycles,instructions,branches,branch-misses,bus-cycles,stalled-cycles-frontend,stalled-cycles-backend,task-clock,page-faults,context-switches,cpu-migrations,L1-dcache-loads,L1-dcache-load-misses,L1-dcache-stores,L1-dcache-store-misses,L1-dcache-prefetch-misses,L1-icache-load-misses,LLC-loads,LLC-stores,LLC-prefetches,dTLB-loads,dTLB-load-misses,dTLB-stores,dTLB-store-misses,iTLB-loads,iTLB-load-misses

perf stat -p $pid -I $sampling_time -e $events -x , -o $results -- sleep $running_time
echo "Done"

