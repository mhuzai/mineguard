import csv
import collections
import sys
import os

data = {}
headers = ['application-name', 'time']

if len(sys.argv) != 3:
	print 'ERROR \nUsage: python input.csv output.csv'
	sys.exit(1)
 
with open(sys.argv[1]) as source:
	next(source, None)
	next(source, None)

	reader = csv.reader(source)
	with open('inter.csv', mode='wb') as inter:
		writer = csv.writer(inter)
		for row in reader:
			writer.writerow((row[0],row[1]))
			if row[3] not in headers:
				headers.append(row[3])

with open('inter.csv') as f:
	reader = csv.reader(f)
	for row in reader:
		t = row[0]
		if t in data:
			data[t].append(row[1])
		else:
			data[t] = [row[1]]


od = collections.OrderedDict(sorted(data.items()))

# get app name from input file and remove directory name from the start and
# .csv from the end
app_name = sys.argv[1]
app_name = app_name.split('/')[-1]
app_name = app_name[:-4]

# so we can write it to file
app_name = app_name.split()
with open(sys.argv[2], mode='wb') as r:
	w = csv.writer(r)
        w.writerow(headers)

	for key, value in od.items():
		t = int(float(key))
		w.writerow(app_name + [t] + value)

os.remove('inter.csv')

