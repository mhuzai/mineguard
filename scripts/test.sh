#!/bin/bash

count=0
while [ $count -lt 10 ];
do
        echo "$count"
        ./../classifier/predict ../data/test${count}.csv ../data/vm-signature.txt
        count=$((count + 1))
done

