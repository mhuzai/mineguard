#include "common.h"

using namespace boost;
using namespace std;

// ./train <training_file> <signature_file>
#define NUM_ARGS 3

int train(const char *training_file, const char *signature_file) {
    // need to manually set number of samples right now but can be automated
    // based on sampling script
    const size_t num_samples = 286;
    const size_t shape[] = {num_samples, num_features};

    // for parsing
    string line;
    size_t sample = 0;

    // create features matrix and labels vector and load them both from csv file
    andres::Marray<Feature> features(shape, shape + 2);
    andres::Marray<Label> labels(shape, shape + 1);
    ifstream ifs(training_file);

    if (!ifs.is_open()) {
        cout << "Error opening training data\n";
        return 1;
    }

    // skip the header
    getline(ifs, line);

    while (getline(ifs, line)) {
        // split on commas
        vector<string> input;
        split(input, line, is_any_of(","));

        // skip first two elements (application name and time) and last
        // element (label)
        size_t feature = 0;
        for (size_t index = 2; index < input.size() - 1; index++, feature++) {
            features(sample, feature) = atol(input[index].c_str());
        }

        // last element is the label
        labels(sample) = atoi(input[input.size() - 1].c_str());

        sample++;
    }

    // learn decision forest
    andres::ml::DecisionForest<Feature, Label, Probability> decision_forest;
    decision_forest.learn(features, labels, num_decision_trees);

    // serialize forest and write out to signature file
    ofstream ofs;
    ofs.open(signature_file, ofstream::out);
    decision_forest.serialize(ofs);
    ofs.close();

    return 0;
}

int main(int argc, char **argv) {
    if (argc != NUM_ARGS) {
        cout << "usage: ./train <training_file> <signature_file>\n";
        return EXIT_FAILURE;
    }

    char *training_file = argv[1];
    char *signature_file = argv[2];
    return train(training_file, signature_file);
}

