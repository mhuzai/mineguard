#include <vector>
#include <fstream>
#include <boost/algorithm/string.hpp>

#include "andres/marray.hxx"
#include "andres/ml/decision-trees.hxx"

// define types for features, labels and probabilities
typedef long Feature;
typedef int Label;
typedef double Probability;

// number of features is fixed
const size_t num_features = 26;

// 50 should be enough
const size_t num_decision_trees = 50;

